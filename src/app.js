import Bingx from "./bingx";
import moment from "moment";
import _ from "lodash";
import BingxApi from "./bingxapi";
import * as dotenv from 'dotenv';
dotenv.config();



main();

/*
data: {
code: 0,
dataType: 'INJ-USDT',
s: 'INJ-USDT',
data: [
    {
        c, - current
        o, - open
        h, - max price
        l, - min price
        v, - volume
        T, - time kandle
        }
        ]
 */

let timer;
let time
let open
let current
let maxPrice
let minPrice

let bingxApi = new BingxApi(process.env.BINGX_KEY,process.env.BINGX_SECRET)
let symbol = 'INJ-USDT'
async function main() {
    let traid = new Bingx(symbol)

    let bingxApi = new BingxApi(process.env.BINGX_KEY,process.env.BINGX_SECRET)

    traid.on('message', function(data) {
        //console.log(JSON.stringify(data))
        time = data.data[0].T
        open = data.data[0].o;
        current = data.data[0].c;
        maxPrice = data.data[0].h;
        minPrice = data.data[0].l;
        let percent = (current * 100) / open
        let fl = (percent - 100).toFixed(2)

        if( moment().second() >= 45 && fl < -2.5) {
            if(timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(() => triggerToBuy(open,current,maxPrice,minPrice),2000);
        }


        //console.log(`${data.s} - ${open} - ${close} - ${fl}% - ${time} now:${moment()}`);
    })
    //traid.start()

    //let resp = await bingxApi.trade({"symbol":"INJ-USDT","type":"MARKET","side":"BUY","positionSide":"LONG","quantity":1})

    let resp = await bingxApi.trade({
        symbol: 'INJ-USDT',
        side: 'BUY',
        type: 'TRAILING_STOP_MARKET',
        positionSide: 'SHORT',
        quantity: 1,
        price: 40.001
    })

    console.log(resp)

    console.log('started');
}

async function triggerToBuy() {
    const low_per = (100 + 15)/100;

    let price_upper = minPrice * low_per

    if( price_upper < current ) return

    console.log({
        open,
        current,
        maxPrice,
        minPrice,
        time: moment().format('m:ss'),
        fl: (((current * 100) / open) - 100).toFixed(2)
    })

    //test
    let resp = await bingxApi.trade({
        symbol: symbol,
        side: 'BUY',
        type: 'TRAILING_STOP_MARKET',
        positionSide: 'SHORT',
        quantity: 1,
        price: current
    })

    let order = resp.data.order;

    //todo
}


//type: {
// LIMIT: Mandatory Parameters: quantity, price
//
// MARKET: Mandatory Parameters: quantity
//
// TRAILING_STOP_MARKET (Tracking Stop Loss Order) or TRAILING_TP_SL (Trailing TakeProfit/StopLoss Order): The price field or priceRate field needs to be filled in
//
// TRIGGER_LIMIT, STOP, TAKE_PROFIT: Mandatory Parameters: quantity、stopPrice、price
//
// STOP_MARKET, TAKE_PROFIT_MARKET, TRIGGER_MARKET: Mandatory Parameters: quantity、stopPrice
// }