import WebSocket from "ws";
import moment from "moment";
import * as zlib from "zlib";
import { v4 as uuidv4 } from 'uuid';

class Bingx {
    socket;
    ws_url;

    eventHandlers = {}

    constructor(pair) {
        this.ws_url = "wss://open-api-swap.bingx.com/swap-market";
        let type = 'kline_1m';
        this.channel = {
            id: uuidv4(),
            reqType: "sub",
            dataType:`${pair}@${type}`
        };
    }

    start() {
        this.socket = new WebSocket(this.ws_url);

        this.socket.on('open', this._open.bind(this));
        this.socket.on('close', this._close.bind(this));
        this.socket.on('message', this._message.bind(this));

    }

    _open() {
        console.log('connected, wait klines');
        this.socket.send(JSON.stringify(this.channel))
    }

    _message(message) {
        const buf = Buffer.from(message);
        const decodedMsg = zlib.gunzipSync(buf).toString('utf-8');
        //console.log(decodedMsg);
        if (decodedMsg === "Ping") {
            this.socket.send('Pong');
            return;
        }

        let data = JSON.parse(decodedMsg);

        if( data.code != 0 ) {
            console.log(data)
        }
        if(!data.data) return;

        let callbacks = this.eventHandlers['message'] ?? [];
        callbacks.forEach(callback => callback(data));

    }

    _close() {
        console.log('websocket closed')
    }


    on(type,handler) {
        if (!this.eventHandlers[type]) { this.eventHandlers[type] = []; }
        this.eventHandlers[type].push(handler);
        return this;
    }
}

export default Bingx