import CryptoJS from "crypto-js";
import axios from "axios";

class BingxApi {
    apiKey
    secretKey

    constructor(apiKey,secretKey) {
        this.apiKey = apiKey
        this.secretKey = secretKey
    }

    sign(parameters,timestamp) {
        return CryptoJS.enc.Hex.stringify(CryptoJS.HmacSHA256(this.getParameters(parameters, timestamp), this.secretKey))
    }

    getParameters(data, timestamp, urlEncode) {
        let parameters = ""
        for (const key in data) {
            if (urlEncode) {
                parameters += key + "=" + encodeURIComponent(data[key]) + "&"
            } else {
                parameters += key + "=" + data[key] + "&"
            }
        }
        if (parameters) {
            parameters = parameters.substring(0, parameters.length - 1)
            parameters = parameters + "&timestamp=" + timestamp
        } else {
            parameters = "timestamp=" + timestamp
        }
        return parameters
    }

    async trade(order) {
        const host = 'https://open-api-vst.bingx.com'; // demo
        //const host = 'https://open-api.bingx.com'; //prod
        const path = `${host}/openApi/swap/v2/trade/order`;

        const timestamp = new Date().getTime()
        let parameters = this.getParameters(order,timestamp)
        const sign = this.sign(order,timestamp)
        const config = {
            method: 'POST',
            url: path+"?"+this.getParameters(order, timestamp, true)+"&signature="+sign,
            headers: {
                'X-BX-APIKEY': this.apiKey,
            },
        }

        const {data} = await axios(config);

        return data;
    }
}

export default BingxApi